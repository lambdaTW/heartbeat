Heartbeat
===
Heartbeat is a package/command tool for the test your server every single second
# Package usage
## Example
### Diagnosis HTTP server and send notification with email and sentry
[Reference](https://gitlab.com/lambdaTW/heartbeat/blob/master/cmd/http.go)

# Command usage
## Build
```
make
```

## Simple Usage
### Setting `config.yaml` file
```
gmail_acc: abc@gmail.com
gmail_pwd: IamPassWord
destination: lambda@lambda.tw
protocol: http
url: http://127.0.0.1:8080
dsn: https://thisisthesentrydns@sentry.io/1111111
```
#### Environment variables
 - `HEARTBEAT_PROTOCOL`
    - The protocol for the heartbeat test
 - `HEARTBEAT_GMAIL_ACC`
    - The gmail account
 - `HEARTBEAT_GMAIL_PASS`
    - The gmail password
 - `HEARTBEAT_DESTINATION`
    - The error receiver
 - `HEARTBEAT_SENTRY_DSN`
    - The sentry Dsn
 - `HEARTBEAT_SOCKET`
    - The heartbeat target for TCP
 - `HEARTBEAT_URL`
    - The heartbeat target for HTTP
#### Example
```shell-script
HEARTBEAT_GMAIL_ACC=abc@gmail.com
HEARTBEAT_GMAIL_PASS=IamPassWord
HEARTBEAT_DESTINATION=lambda@lambda.tw
HEARTBEAT_DSN=https://thisisthesentrydns@sentry.io/1111111
HEARTBEAT_URL=http://127.0.0.1:8080
```
### Run
```shell-script
# HTTP
heartbeat http
# TCP
heartbeat tcp
```

## Use docker
```
# Create docker image
make docker

# Run container with config.yaml file
docker run --rm -v $PWD/config.yaml:/config.yaml heartbeat:latest
```

## As systemd
### Install
```
sudo make install
```
### Run
```
# Use project/path/config.yaml
# Log to /var/log/heartbeat.log
systemctl start heartbeat.service

# Use project/path/site/<you defined path name>/config.yaml
# Log to /var/log/heartbeat/heartbeat-<you defined path name>/heartbeat.log
systemctl start heartbeat@<you defined path name>.service
```
