package heartbeat

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/lambdaTW/heartbeat/pkg/connector"
	"gitlab.com/lambdaTW/heartbeat/pkg/handler"
)

// Heartbeat will try connect every single second until stop it
type Heartbeat struct {
	connector.Connector
	Handlers []handler.Handler
	context.Context
}

// Start the Heartbeat without wait any connect
func (h *Heartbeat) Start() {
	go func() {
		for {
			select {
			case <-time.Tick(1 * time.Second):
				ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
				defer cancel()
				result := make(chan error, 1)
				go func() {
					result <- h.Connector.Connect()
				}()
				go func() {
					var err error
					select {
					case err = <-result:
					case <-ctx.Done():
						err = fmt.Errorf("Connect timeout")
					}
					if err != nil {
						for _, handler := range h.Handlers {
							handler.Handle(err.Error())
						}
					}

				}()
			case <-h.Context.Done():
				return
			default:
			}
		}
	}()
}

// StartSync start Heartbeat test the target and wait for the last connection return
func (h *Heartbeat) StartSync() {
	go func() {
		for {
			select {
			case <-time.Tick(1 * time.Second):
				if err := h.Connector.Connect(); err != nil {
					for _, handler := range h.Handlers {
						handler.Handle(err.Error())
					}
				}
			case <-h.Context.Done():
				return
			default:
			}

		}
	}()
}
