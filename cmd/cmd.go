package main

import (
	"fmt"

	sentry "github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lambdaTW/heartbeat/pkg/handler"
	"gitlab.com/lambdaTW/heartbeat/pkg/handler/handlers"
	"gitlab.com/lambdaTW/heartbeat/pkg/loggy"
	"gitlab.com/lambdaTW/heartbeat/pkg/loggy/loggies"
	"gitlab.com/lambdaTW/heartbeat/pkg/notify/notifiers"
)

const (
	envPrefix = "HEARTBEAT"
)

var (
	// For command bind
	cfgFile      string
	protocol     string
	sentryDSN    string
	appName      string
	mailAccount  string
	mailPassword string
	target       string
	ping, pong   string
	stopCode     int
	// For init heartbeat
	delay             = 30
	mailNotify        *notifiers.MailNotifier
	logHandler        *handlers.LogHandler
	logger            loggy.Loggy
	heartbeatHandlers []handler.Handler
)

func initHeartbeat() {
	logger = &loggies.AppLoggy{AppName: viper.GetString("app_name")}
	mailNotify = notifiers.NewGmailNotify(
		viper.GetString("gmail_acc"),
		viper.GetString("gmail_pwd"),
		viper.GetString("destination"),
		logger,
	)
	logHandler = &handlers.LogHandler{
		Delay:    delay,
		Try:      0,
		SendTime: 0,
		Loggy:    logger,
		Notifier: mailNotify,
		LastLogs: make(chan string, 30),
	}
	heartbeatHandlers = []handler.Handler{
		logHandler,
	}
	if viper.GetString("sentry_dsn") != "" {
		options := sentry.ClientOptions{
			Dsn: viper.GetString("sentry_dsn"),
		}
		sentryHandler := handlers.NewSentryHandler(options)
		heartbeatHandlers = append(heartbeatHandlers, sentryHandler)
	}
}

var rootCmd = &cobra.Command{
	Use:   "heartbeat",
	Short: "The heartbeat check the server",
	Long:  `The heartbeat check the server with select protocol every single time`,
	Run: func(cmd *cobra.Command, args []string) {
		if protocol := viper.GetString("protocol"); protocol != "" {
			switch protocol {
			case "http":
				startHTTPHeartbeat()
			case "tcp":
				startTCPHeartbeat()
			default:
				fmt.Printf("Not support protocol: %s", protocol)
			}
		}
	},
}

var tcpCmd = &cobra.Command{
	Use:   "tcp",
	Short: "Create the TCP heartbeat",
	Long:  "Check the TCP socket with PING string every single time",
	Run: func(cmd *cobra.Command, args []string) {
		startTCPHeartbeat()
	},
}

var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Create the HTTP heartbeat",
	Long:  `Check the HTTP server with GET request every single time`,
	Run: func(cmd *cobra.Command, args []string) {
		startHTTPHeartbeat()
	},
}

type flag struct {
	Bind       *string
	Name       string
	Short      string
	Default    string
	Long       string
	Required   bool
	Persistent bool
}

func bindArg(cmd *cobra.Command, f *flag) {
	flagSet := cmd.Flags()
	if f.Persistent {
		flagSet = cmd.PersistentFlags()
	}
	if f.Short == "" {
		flagSet.StringVar(f.Bind, f.Name, f.Default, f.Long)
	} else {
		flagSet.StringVarP(f.Bind, f.Name, f.Short, f.Default, f.Long)
	}
	if f.Required {
		cmd.MarkFlagRequired(f.Name)
	}
	viper.BindPFlag(f.Name, flagSet.Lookup(f.Name))
}

func init() {
	rootCmd.AddCommand(httpCmd)
	rootCmd.AddCommand(tcpCmd)

	viper.SetEnvPrefix(envPrefix)
	viper.BindEnv("protocol")
	viper.BindEnv("gmail_acc")
	viper.BindEnv("gmail_pwd")
	viper.BindEnv("destination")
	viper.BindEnv("sentry_dsn")
	viper.BindEnv("app_name")
	viper.BindEnv("url")
	viper.BindEnv("socket")

	appName = viper.GetString("app_name")
	if appName == "" {
		appName = "heartbeat"
	}

	for _, f := range []*flag{
		&flag{
			Bind:    &protocol,
			Name:    "protocol",
			Short:   "p",
			Default: "http",
		},
		&flag{
			Bind:       &sentryDSN,
			Name:       "dsn",
			Long:       "Sentry DSN",
			Persistent: true,
		},
		&flag{
			Bind:       &mailAccount,
			Name:       "gmail_acc",
			Long:       "The google account for sending email",
			Persistent: true,
		},
		&flag{
			Bind:       &mailPassword,
			Name:       "gmail_pwd",
			Long:       "The google account password for sending email",
			Persistent: true,
		},
		&flag{
			Bind:       &appName,
			Name:       "name",
			Short:      "n",
			Default:    appName,
			Long:       "The APP name for log",
			Persistent: true,
		},
	} {
		bindArg(rootCmd, f)
	}

	bindArg(httpCmd, &flag{
		Bind: &target,
		Name: "url",
		Long: "The URL for the heartbeat test",
	})

	bindArg(tcpCmd, &flag{
		Bind: &target,
		Name: "socket",
		Long: "The TCP socket for heartbeat test",
	})

	bindArg(tcpCmd, &flag{
		Bind:    &ping,
		Default: "PING",
		Name:    "ping",
		Long:    "The TCP ping string for heartbeat test",
	})

	bindArg(tcpCmd, &flag{
		Bind:    &pong,
		Default: "PONG",
		Name:    "pong",
		Long:    "The TCP pong string for heartbeat test",
	})
	tcpCmd.PersistentFlags().IntVarP(
		&stopCode,
		"stop", "s",
		10,
		"The TCP stop code for heartbeat test, default is 10 (code of \\n)",
	)
	viper.BindPFlag("stop", tcpCmd.Flags().Lookup("stop"))

	cobra.OnInitialize(func() {
		if cfgFile != "" {
			viper.SetConfigFile(cfgFile)
		} else {
			viper.AddConfigPath("./")
			viper.SetConfigName("config")
		}
		viper.ReadInConfig()
	})
}
