package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/spf13/viper"
	"gitlab.com/lambdaTW/heartbeat"
	"gitlab.com/lambdaTW/heartbeat/pkg/connector/connectors"
)

func startTCPHeartbeat() {
	initHeartbeat()
	target = viper.GetString("socket")
	ping := viper.GetString("ping")
	pong := viper.GetString("pong")
	conn, err := net.Dial("tcp", target)
	if err != nil {
		log.Fatalf("Cannot connect to the %s", target)
	}
	defer conn.Close()

	connector := &connectors.TCPConnector{
		Target: target,
		Conn:   conn,
		Ping:   ping,
		Pong:   pong,
		Stop:   10, // '\n'
	}
	logHandler.Connector = connector
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	hb := heartbeat.Heartbeat{
		Connector: connector,
		Handlers:  heartbeatHandlers,
		Context:   ctx,
	}
	hb.Start()
	fmt.Println("Heartbeat start test", target)

	<-ctx.Done()
}
