package main

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"

	_ "github.com/joho/godotenv/autoload"
	"github.com/spf13/viper"
	"gitlab.com/lambdaTW/heartbeat"
	"gitlab.com/lambdaTW/heartbeat/pkg/connector/connectors"
	"gitlab.com/lambdaTW/heartbeat/pkg/handler/handlers"
	gomail "gopkg.in/gomail.v2"
)

var (
	domain      string
	targetURL   *url.URL
	ips         []net.IP
	client      *http.Client
	req         *http.Request
	try         = 0
	sendTime    = 0
	lastLog     = make(chan string, 30)
	mailChannel = make(chan *gomail.Message)
)

func initHTTPHeartbeat() {
	initHeartbeat()

	var err error
	target = viper.GetString("url")
	targetURL, _ = url.Parse(target)
	domain = targetURL.Scheme + "://" + targetURL.Host
	ips, err = net.LookupIP(targetURL.Hostname())
	if err != nil {
		log.Panicf("Error, cannot lookup the host: %s IP", domain)
	}
	client = new(http.Client)
	req, err = http.NewRequest("GET", target, bytes.NewBuffer([]byte("")))
	if err != nil {
		panic("Cannot build HTTP request")
	}

	if viper.GetString("gmail_acc") == "" ||
		viper.GetString("gmail_pwd") == "" {
		logger.Log("Cannot send the email notification cause by the providing email account information not complete")
	}
}

func startHTTPHeartbeat() {
	initHTTPHeartbeat()
	heartbeatHandlers = append(
		heartbeatHandlers,
		&handlers.IPHandler{
			Loggy: logger,
			URL:   targetURL,
			IPs:   ips,
		})
	connector := connectors.NewTrackHTTPConnector(
		targetURL,
		req,
		domain,
		ips,
		heartbeatHandlers,
		logger,
	)
	logHandler.Connector = connector
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	hb := heartbeat.Heartbeat{
		Connector: connector,
		Handlers:  heartbeatHandlers,
		Context:   ctx,
	}
	fmt.Println("Heartbeat start test", target)
	hb.Start()
	<-ctx.Done()
}
