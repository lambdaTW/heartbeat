PROJECT_NAME = heartbeat
PKG := "gitlab.com/lambdaTW/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

RSYSLOG_CONF_PATH = init/rsyslog
SYSTEMD_CONF_PATH = init/systemd
SYSTEMD_PATH = /lib/systemd/system

LOGROTATE_FILE = /etc/logrotate.d/heartbeat
RSYSLOG_FILE = /etc/rsyslog.d/99-heartbeat.conf

.PHONY: all clean install uninstall docker lint test race msan coverage coverhtml dep build help conf

all: build

lint: ## Lint the files
	go get -u golang.org/x/lint/golint
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@go build -i -v -o $(PROJECT_NAME) $(PKG)/cmd

clean: ## Remove previous build
	rm -f $(PROJECT_NAME) $(PROJECT_NAME).service $(PROJECT_NAME)@.service
	rm -f $(PROJECT_NAME)-rotate.conf

install: conf ## Install configurations to system
	useradd $(PROJECT_NAME) -s /sbin/nologin -M || true

	mv $(PROJECT_NAME).service $(PROJECT_NAME)@.service $(SYSTEMD_PATH)/.
	chmod 744 $(SYSTEMD_PATH)/$(PROJECT_NAME).service
	chmod 744 $(SYSTEMD_PATH)/$(PROJECT_NAME)@.service

	mv $(PROJECT_NAME)-rotate.conf $(LOGROTATE_FILE)
	mv $(RSYSLOG_CONF_PATH)/rsyslog.conf $(RSYSLOG_FILE)

uninstall: ## Remove configurations on system
	-rm -rf $(SYSTEMD_PATH)/$(PROJECT_NAME).service $(SYSTEMD_PATH)/$(PROJECT_NAME)@.service
	-rm -rf $(LOGROTATE_FILE)
	-rm -rf $(RSYSLOG_FILE)
	-make clean

docker: ## Create docker image
	docker build -t $(PROJECT_NAME) -f build/docker/Dockerfile .

conf: $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME).service $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME)@.service $(RSYSLOG_CONF_PATH)/logrotate.conf ## Generate configurations
	@echo "Make $(PROJECT_NAME).service from $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME).service"
	@envsubst < $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME).service > $(PROJECT_NAME).service

	@echo "Make $(PROJECT_NAME)@.service from $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME)@.service"
	@envsubst < $(SYSTEMD_CONF_PATH)/$(PROJECT_NAME)@.service > $(PROJECT_NAME)@.service

	@echo "Make $(PROJECT_NAME)-rotate.conf from $(RSYSLOG_CONF_PATH)/logrotate.conf"
	@envsubst < $(RSYSLOG_CONF_PATH)/logrotate.conf > $(PROJECT_NAME)-rotate.conf

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
