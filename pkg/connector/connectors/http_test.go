package connectors

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/lambdaTW/heartbeat/pkg/handler"
	"gitlab.com/lambdaTW/heartbeat/pkg/loggy/loggies"
)

type mockClient struct {
	Code int
}
type mockBody struct{}

func (mb *mockBody) Read(p []byte) (n int, err error) {
	return 0, io.EOF
}

func (mb *mockBody) Close() error {
	return nil
}

func (mc *mockClient) Do(req *http.Request) (*http.Response, error) {
	return &http.Response{
		StatusCode: mc.Code,
		Body:       new(mockBody),
	}, nil
}

func TestConnect(t *testing.T) {
	t.Run("Test 200, 201, 204 status code is pass", func(t *testing.T) {
		for _, code := range []int{200, 201, 204} {
			hc := &HTTPConnector{
				Client: &mockClient{
					Code: code,
				},
				Request: &http.Request{
					URL: new(url.URL),
				},
			}
			err := hc.Connect()
			if err != nil {
				msg := fmt.Sprintf("The response from server return the code %d not valid in function Connect, %s", code, err.Error())
				t.Error(msg)
			}
		}
	})
}

func TestNewTrackHTTPConnector(t *testing.T) {
	t.Run("Test NewTrackHTTPConnector will append the trace value into context",
		func(t *testing.T) {
			req := new(http.Request)
			ctx := req.Context()

			httpConnector := NewTrackHTTPConnector(
				new(url.URL),
				req,
				"https://domain.com",
				[]net.IP{},
				[]handler.Handler{},
				new(loggies.AppLoggy),
			)
			if httpConnector.Request.Context() == ctx {
				t.Error("The new track HTTP connector do not give the request with the trace context")
			}
		})
}
