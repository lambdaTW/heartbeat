package connectors

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptrace"
	"net/url"
	"strings"
	"time"

	"gitlab.com/lambdaTW/heartbeat/pkg/handler"
	"gitlab.com/lambdaTW/heartbeat/pkg/loggy"
)

var validCode = []int{200, 201, 204}

// RequestHandler provide the Do function for process *http.Request
type RequestHandler interface {
	Do(*http.Request) (*http.Response, error)
}

// HTTPConnector provide HTTP connect
type HTTPConnector struct {
	*url.URL
	Client RequestHandler
	*http.Request
	Domain string
}

// Connect provide HTTP connect to the URL
func (hc *HTTPConnector) Connect() error {
	hc.Request.URL.RawQuery = fmt.Sprintf("time=%d", time.Now().Unix())
	resp, err := hc.Client.Do(hc.Request)
	if err != nil {
		return err
	}
	return func() error {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		invalid := true
		for _, code := range validCode {
			if resp.StatusCode == code {
				invalid = false
			}
		}
		if len(body) < 0 || invalid {
			return fmt.Errorf("Cannot connect to %s, Response: %d %s", hc.URL, resp.StatusCode, body)
		}
		return nil
	}()
}

// String return Connect information
func (hc *HTTPConnector) String() string {
	return hc.Domain
}

// NewTrackHTTPConnector create a HTTP connector with request trace
func NewTrackHTTPConnector(
	url *url.URL,
	request *http.Request,
	domain string,
	ips []net.IP,
	handlers []handler.Handler,
	logger loggy.Loggy,
) *HTTPConnector {
	trace := &httptrace.ClientTrace{
		GotConn: func(connInfo httptrace.GotConnInfo) {
			socket := connInfo.Conn.RemoteAddr().String()
			ipStr := strings.Split(socket, ":")[0]
			addr := net.ParseIP(ipStr)
			isNewIP := true
			for _, ip := range ips {
				if ip.Equal(addr) {
					isNewIP = false
				}
			}
			if isNewIP {
				msg := fmt.Sprintf("Got return from new IP %s", addr)
				for _, handler := range handlers {
					handler.Handle(msg)
				}
				logger.Log(msg)
			}
		},
	}
	request = request.WithContext(httptrace.WithClientTrace(request.Context(), trace))
	return &HTTPConnector{
		URL:     url,
		Client:  &http.Client{},
		Request: request,
		Domain:  domain,
	}
}
