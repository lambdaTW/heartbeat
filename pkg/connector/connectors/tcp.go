package connectors

import (
	"bufio"
	"fmt"
	"net"
)

// TCPConnector provide the Connect function for checking
// the TCP Server reply the expecting string
type TCPConnector struct {
	Target string
	net.Conn
	Ping string
	Pong string
	Stop byte
	ping string
	pong string
}

// Connect to the server and check return is accepted
func (tc *TCPConnector) Connect() error {
	if tc.ping == "" {
		tc.ping = tc.Ping + string(tc.Stop)
	}
	if _, err := tc.Conn.Write([]byte(tc.ping)); err != nil {
		return err
	}
	reply, err := bufio.NewReader(tc.Conn).ReadString(tc.Stop)
	if err == nil {
		resp := string(reply)
		if tc.pong == "" {
			tc.pong = (tc.Pong + string(tc.Stop))
		}
		if resp != tc.pong {
			return fmt.Errorf("Get the unexpected string %s, it should be %s", resp, tc.Pong)
		}
	}
	return err
}

func (tc *TCPConnector) String() string {
	return fmt.Sprintf("Connect to %s with ping: %s, pong: %s, end with %d", tc.Target, tc.Ping, tc.Pong, tc.Stop)
}
