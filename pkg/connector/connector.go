package connector

// Connector provide connect for protocol
type Connector interface {
	Connect() error
	String() string
}
