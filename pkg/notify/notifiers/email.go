package notifiers

import (
	"fmt"
	"time"

	"gitlab.com/lambdaTW/heartbeat/pkg/loggy"

	gomail "gopkg.in/gomail.v2"
)

const (
	// SMTPPort is the constant port for SMTP
	SMTPPort = 587
)

// SimpleMailNotifier provide the sync way Notify
type SimpleMailNotifier struct {
	SMTPServer  string
	Account     string
	Password    string
	Destination string
}

// Notify implement the Notify for the mail send with sync
func (smn *SimpleMailNotifier) Notify(subject string, log <-chan string) {
	body := ""
	logLen := len(log)
	for i := 0; i < logLen; i++ {
		body = body + "<br>" + <-log
	}
	m := gomail.NewMessage()
	m.SetHeader("From", smn.Account)
	m.SetHeader("To", smn.Destination)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)
	fmt.Println("Send mail to:", m.GetHeader("To"))
	d := gomail.NewDialer(smn.SMTPServer, SMTPPort, smn.Account, smn.Password)
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
}

// MailNotifier is implement simple email notify
type MailNotifier struct {
	Account     string
	Password    string
	Destination string
	postbox     chan *gomail.Message
}

// Notify send email
func (mn *MailNotifier) Notify(subject string, log <-chan string) {
	body := ""
	logLen := len(log)
	for i := 0; i < logLen; i++ {
		body = body + "<br>" + <-log
	}
	m := gomail.NewMessage()
	m.SetHeader("From", mn.Account)
	m.SetHeader("To", mn.Destination)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)
	fmt.Println("Send mail to:", m.GetHeader("To"))
	mn.postbox <- m
}

// NewGmailNotify create new email notify use gmail server
func NewGmailNotify(account, password, destination string, loggy loggy.Loggy) *MailNotifier {
	postbox := make(chan *gomail.Message)

	go func() {
		d := gomail.NewDialer("smtp.gmail.com", SMTPPort, account, password)

		var sendCloser gomail.SendCloser
		var err error
		open := false
		for {
			select {
			case m, ok := <-postbox:
				if !ok {
					return
				}
				if !open {
					if sendCloser, err = d.Dial(); err != nil {
						loggy.Log("error", err)
					}
					open = true
				}
				if err := gomail.Send(sendCloser, m); err != nil {
					loggy.Log("error", err)
				}
				loggy.Log("Sent")
			// Close the connection to the SMTP server if no email was sent in
			// the last 30 seconds.
			case <-time.After(30 * time.Second):
				if open {
					if err := sendCloser.Close(); err != nil {
						panic(err)
					}
					open = false
				}
			}
		}
	}()
	return &MailNotifier{
		Account:     account,
		Password:    password,
		Destination: destination,
		postbox:     postbox,
	}
}
