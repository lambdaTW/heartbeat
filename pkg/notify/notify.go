package notify

// Notifier for notify the message to receiver
type Notifier interface {
	Notify(subject string, body <-chan string)
}
