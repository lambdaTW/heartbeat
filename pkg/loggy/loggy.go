package loggy

// Loggy is an interface have a customize Log function
type Loggy interface {
	Log(logs ...interface{})
}
