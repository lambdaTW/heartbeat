package loggies

import "log"

// AppLoggy provide the log with the app name
type AppLoggy struct {
	AppName string
}

// Log for AppLoggy is always prepend the app name
func (al *AppLoggy) Log(logs ...interface{}) {
	log.Println(al.AppName, logs)
}
