package handlers

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/lambdaTW/heartbeat/pkg/connector"
	"gitlab.com/lambdaTW/heartbeat/pkg/loggy"
	"gitlab.com/lambdaTW/heartbeat/pkg/notify"
)

// LogHandler can store last log and send notify
type LogHandler struct {
	Delay    int
	Try      int
	SendTime int
	loggy.Loggy
	notify.Notifier
	connector.Connector
	LastLogs chan string
}

// Handle is Loghandler's Handle implement
func (h *LogHandler) Handle(accident string) {
	h.Loggy.Log("error", accident)
	if len(h.LastLogs) == 30 {
		<-h.LastLogs
	}
	h.LastLogs <- fmt.Sprint(time.Now().UTC(), " ", accident)
	// Increase delay until more than 1 hour
	delay := float64(h.Delay) * math.Pow(2, float64(h.SendTime))
	errorMoreThanDelay := float64(h.Try) > delay
	if errorMoreThanDelay ||
		h.Try > 60*60 {
		subject := fmt.Sprintf("%s cannot be reached", h.Connector.String())
		h.Notifier.Notify(
			subject,
			h.LastLogs,
		)
		h.SendTime++
		h.Try = 0
		fmt.Println("Send time: ", h.SendTime)
	}

	h.Try++
}
