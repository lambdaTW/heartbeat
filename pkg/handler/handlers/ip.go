package handlers

import (
	"net"
	"net/url"

	"gitlab.com/lambdaTW/heartbeat/pkg/loggy"
)

// IPHandler for check IPs change
type IPHandler struct {
	loggy.Loggy
	*url.URL
	IPs []net.IP
}

// Handle is IPHandler's Handle implement
func (h *IPHandler) Handle(accident string) {
	newIPs, _ := net.LookupIP(h.URL.Hostname())
	stableIPs := []net.IP{}
	removedIPs := []net.IP{}
	_newIPs := []net.IP{}
	for _, ip := range h.IPs {
		stable := false
		for _, newIP := range newIPs {
			if ip.Equal(newIP) {
				stable = true
			}
		}
		if stable {
			stableIPs = append(stableIPs, ip)
		} else {
			removedIPs = append(removedIPs, ip)
		}
	}
	for _, ip := range newIPs {
		new := true
		for _, stableIP := range stableIPs {
			if ip.Equal(stableIP) {
				new = false
			}
		}
		if new {
			_newIPs = append(_newIPs, ip)
		}
	}
	if len(_newIPs) > 0 {
		h.Loggy.Log("New IPs", newIPs)
	}
	if len(removedIPs) > 0 {
		h.Loggy.Log("Expired IPs", removedIPs)
	}
	h.Loggy.Log("Old IPs", h.IPs)
	h.Loggy.Log("Current IPs", newIPs)
	h.IPs = newIPs
}
