package handlers

import (
	"fmt"

	sentry "github.com/getsentry/sentry-go"
)

// SentryHandler handle the accident to sentry DSN
type SentryHandler struct {
}

// Handle is SentryHandler implement
func (sh *SentryHandler) Handle(accident string) {
	sentry.CaptureException(fmt.Errorf(accident))
}

// NewSentryHandler to init sentry hub
func NewSentryHandler(options sentry.ClientOptions) *SentryHandler {
	sentry.Init(options)
	return &SentryHandler{}
}
