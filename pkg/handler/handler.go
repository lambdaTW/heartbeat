package handler

// Handler have customize function for handle error
type Handler interface {
	Handle(accident string)
}
